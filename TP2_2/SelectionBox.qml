import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "Dnd.js" as Data
Rectangle{
    width: parent.width;
    property string nomCarre:"unNom"
    height: 80;
    color: "#00000000"
    // red - 6d2b2b
    // brown - 2b2823
    Button{
        anchors.centerIn: parent;
        //color: "#666"
        width: parent.width-40

        background: Rectangle {
            id: buttonBg
            color: "#2b2823";
            width: parent.width;
            height: 110;
            radius:10;
        }
        Rectangle{
            color: "white";
            anchors.top: buttonBg.top;
            anchors.topMargin: 15;
            anchors.left: buttonBg.left;
            anchors.leftMargin: 15;
            height: 80;
            width: 90;
            radius:10;

            Rectangle{
                color: "white";
                anchors.bottom: parent.bottom;
                anchors.bottomMargin: 0;
                height: 50;
                width: parent.width;
                visible: false;
            }
        }

        Label{
            id: carreText
            text: nomCarre
            color: "#fff"
            anchors.left: parent.left;
            anchors.leftMargin: 120;
            anchors.top: parent.top;
            anchors.topMargin: buttonBg.height/2 - height/2;
            font.pixelSize: 28;
        }
    }
}
