import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import "Dnd.js" as Data

Window {
    visible: true
    width: 410
    height: 725
    title: qsTr("DnD")
    id:appWindow

    function load(){
    }

    ScrollView{
        anchors.fill: parent

        ListView{
            anchors.centerIn: parent
            id:liste
            spacing: 40
            orientation: Qt.Vertical
            model:Data.Races.length
            delegate:SelectionBox{nomCarre:Data.Races[index]}

            header: Rectangle{
                height: 60;
                width: parent.width;
                color: "#2b2823";
            }
        }
    }
        Component.onCompleted: load();
}
